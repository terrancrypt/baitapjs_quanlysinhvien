// Mảng chính
var DSSV = "DSSV";
var dssv = [];
// Lấy dữ liệu từ localStorage nếu có
// Object lấy từ localStorage sẽ bị mất các key chứa function (method)
var dssvJson = localStorage.getItem(DSSV);
// Convert json to array
if (dssvJson != null) {
  var svArr = JSON.parse(dssvJson);
  // Chuyển đổi array chứa object không có key tinhDTB() thành array chứa object có key tinhDTB()
  dssv = svArr.map(function (item) {
    return (sv = new SinhVien(
      item.ma,
      item.ten,
      item.email,
      item.matKhau,
      item.diemToan,
      item.diemLy,
      item.diemHoa
    ));
  });
  renderDSSV(dssv);
}

//Thêm sinh viên
function themSinhVien() {
  // Dùng hàm nãy sẽ trả về kết quả là 1 Object chứa thông tin của tất cả sinh viên
  var sv = layThongTinTuForm();
  // Push object đã lấy từ form vào array dssv đã tạo ban đầu
  dssv.push(sv);
  // Sau khi lấy thông tin sinh viên thành công thì bắt đầu thêm nó vào trong localStorage
  // Chuyển đổi array đã lấy được thành Json
  var dssvJson = JSON.stringify(dssv);
  // Lưu dữ liệu Json đã được chuyển đổi vào localStorage
  localStorage.setItem(DSSV, dssvJson);
  // Sau khi lưu vào localStorage hoàn tất thì render dssv ra màn hình
  renderDSSV(dssv);
}

// Xóa sinh viên
function xoaSinhVien(idSV) {
  //  tìm vị trí
  var viTri = timKiemViTri(idSV, dssv);
  if (viTri != -1) {
    dssv.splice(viTri, 1);
    // render lại layout sau khi xóa thành công
    renderDSSV(dssv);
    // Lưu lại vào localStorage
    var dssvJson = JSON.stringify(dssv);
    localStorage.setItem(DSSV, dssvJson);
  }
}

// Sửa sinh viên
function suaSinhVien(idSv) {
  var viTri = timKiemViTri(idSv, dssv);
  if (viTri == -1) {
    // Dừng chương trình nếu không tìm thấy
    return;
  }
  var sv = dssv[viTri];
  // Show thông tin lên form
  showThongTinLenForm(sv);
  // Lưu lại vào localStorage
  var dssvJson = JSON.stringify(dssv);
  localStorage.setItem(DSSV, dssvJson);
}

// Cập nhật sinh viên
function capNhatSinhVien() {
  // Lấy thông tin sau khi user update
  var sv = layThongTinTuForm();
  // Update dữ liệu mới thay thế dữ liệu cũ
  var viTri = timKiemViTri(sv.ma, dssv);
  if (viTri != -1) {
    dssv[viTri] = sv;
    // Render lại layout sau khi update
    renderDSSV(dssv);
    // Lưu lại vào localStorage
    var dssvJson = JSON.stringify(dssv);
    localStorage.setItem(DSSV, dssvJson);
  }
}
