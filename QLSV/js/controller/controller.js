function layThongTinTuForm() {
  // Lấy thông tin từ form
  const _maSv = document.getElementById("txtMaSV").value;
  const _tenSv = document.getElementById("txtTenSV").value;
  const _email = document.getElementById("txtEmail").value;
  const _matKhau = document.getElementById("txtPass").value;
  const _diemToan = document.getElementById("txtDiemToan").value * 1;
  const _diemLy = document.getElementById("txtDiemLy").value * 1;
  const _diemHoa = document.getElementById("txtDiemHoa").value * 1;

  // Tạo Object sv
  var sv = new SinhVien(
    _maSv,
    _tenSv,
    _email,
    _matKhau,
    _diemToan,
    _diemLy,
    _diemHoa
  );
  return sv;
}

function renderDSSV(svArr) {
  // Render dssv ta table
  var contentHTML = "";
  for (var index = 0; index < svArr.length; index++) {
    var item = svArr[index];
    var contentTr = `
    <tr>
       <td> ${item.ma} </td>
       <td> ${item.ten} </td>
       <td> ${item.email} </td>
       <td> ${item.tinhDTB()} </td>
       <td> <button class="btn btn-danger" onclick="xoaSinhVien('${
         item.ma
       }')" >Xóa</button>
       <button class="btn btn-warning" onclick="suaSinhVien('${
         item.ma
       }')" >Sửa</button>
        </td>
    </tr>
    `;
    contentHTML += contentTr;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function timKiemViTri(id, arr) {
  var viTri = -1;
  for (var index = 0; index < arr.length; index++) {
    var sv = arr[index];
    if (sv.ma == id) {
      viTri = index;
      break;
    }
  }
  return viTri;
}

function showThongTinLenForm(sv) {
  document.getElementById("txtMaSV").value = sv.ma;
  document.getElementById("txtTenSV").value = sv.ten;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtPass").value = sv.matKhau;
  document.getElementById("txtDiemToan").value = sv.diemToan;
  document.getElementById("txtDiemLy").value = sv.diemLy;
  document.getElementById("txtDiemHoa").value = sv.diemHoa;
}
